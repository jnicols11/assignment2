/*
 * This class is designed to get the parameters of one order.  It is sent to a non default constructor and contains
 * getters and setters for each of the properties
 */
package beans;

public class Order {
	String orderNo = "";
	String productName = "";
	float price = 0;
	int quantity = 0;
	
	public Order(String orderNo, String productName, float price, int quantity) {
		this.orderNo = orderNo;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}//end non-default constructor 
	
	//getters and setters
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}//end getters and setters

}//end class order
