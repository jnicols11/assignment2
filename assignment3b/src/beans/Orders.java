/*
 * This class is designed to create a list of orders with various inputs, the list is stored in an array list and has
 * getters and setters according to the properties
 */
package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class Orders {
	
	//creates a list of order(s) called orders
	List<Order> orders = new ArrayList<Order>();
	
	public Orders() {
		orders.add(new Order("0000000000", "This is Product 1", 1.00f, 1));
		orders.add(new Order("0000000001", "This is Product 2", 1.00f, 1));
		orders.add(new Order("0000000002", "This is Product 3", 1.00f, 1));
		orders.add(new Order("0000000003", "This is Product 4", 1.00f, 1));
		orders.add(new Order("0000000004", "This is Product 5", 1.00f, 1));
		orders.add(new Order("0000000005", "This is Product 6", 1.00f, 1));
		orders.add(new Order("0000000006", "This is Product 7", 1.00f, 1));
		orders.add(new Order("0000000007", "This is Product 8", 1.00f, 1));
		orders.add(new Order("0000000008", "This is Product 9", 1.00f, 1));
		orders.add(new Order("0000000009", "This is Product 10", 1.00f, 1));
	}//end non default constructor
	
	public List<Order> getOrders(){
		return orders;
	}//end getOrders
	
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}//end setOrders
}
