/*
 * This class is designed to create a user with a first and last name using the NotNull annotation to ensure 
 * the data entry is not null and the size annotation to set the minimum and maximum string length
 * the class also contains a default constructor the sets the first and last name to my own
 */
package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class User {
	
	@NotNull(message = "The first name is a required field.")
	@Size(min=5, max=15)
	private String firstname;
	
	@NotNull(message = "The last name is a required field.")
	@Size(min=5, max=15)
	private String lastname;
	
	public User()
	{
		firstname = "Jordan";
		lastname = "Nicols";
	}//end default constructor

	public String getFirstname() {
		return firstname;
	}//end getFirstName
	
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}//end setFirstName
	
	public String getLastname() {
		return lastname;
	}//end getLastName
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}//end setLastName

}//end class user
